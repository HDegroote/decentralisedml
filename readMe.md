# Decentralised ML
Illustrations of different system architectures for applying ML, both centralised and decentralised/distributed, written in node.js and using TensorFlow.js.


### Setup

    npm install

Note: tested in Node v16

### Running example

The running example is a simple 2D function, approximated with binary logistic regression. 
All points to the right of the y-axis are mapped to class 1, and all points to the left are mapped to class 0.

To illustrate the impact of decentralisation in terms of solution quality, the training data is split accross two entities (users).
- User 1: upper right and lower left quadrant
- User 2: lower right and upper left quadrant

This implies that if the two different entities do not pool their data in some way, each will most likely learn a poor approximation when evaluated on the entire instance space.

Schematic illustration of the function and which entity has access to which quadrants of training data:
![Illustration of the example function](artificial_example_illustration.png "Illustration of the example function")


The running example is evaluated for each system architecture on 4 data points, 1 each in each quadrant. 


## Fully centralised system

This is the behaviour of a classic, fully-centralised system. 
One central entity owns the data of all users, as well as the ML models trained with the data. 
The central entity chooses who to share which data and which models with.
The vast majority of systems follow this model.

The main advantage is that it is straightforward for the central entity to train models and perform data analysis, as all data is readily available.

The main disadvantage is that the users do not control their data. They do not know what is stored, and who their data is shared with. 

Another disadvantage is that the user does not know why the model makes certain predictions, because he does not know how the model was trained, and he might not even be aware which of his data the predictions are based on.

##### Illustrations:
- Minimal example:

        node illustrations/illustration_fully_centralised.js 

- Minimal example with a server:
    
        node illustrations/illustration_fully_centralised_with_server.js 
    
Note that since all data is available, the trained model works well. 

## Fully decentralised system - uncooperative entities 

This is the other extreme, comapred to a fully centralised system.
In this setting there are multiple entities, each with their own data, and each entity keeps its data private.

The obvious advantage is that the data is kept safe and private.

The obvious disadvantage is that the data of other entities cannot be used to improve the models' predictions

##### Illustration:
    
    node illustrations/illustration_fully_decentralised_no_collaboration

The running example illustrates how both entities train a model with much lower accuracy than if they had shared their data, 
because each has access to examples of only half the instance space. Unfortunately it is often not possible to share the data, for example if it contains sensitive personal information. 

The following systems illustrate the behaviour of other architectures, falling in between a fully centralised system and a fully decentralised system without collaboration.


## Trusted centralised model - decentralised data

In this system design, each entity stores its own data, rather than storing it with the central entity. The central entity only stores the model. With this design, each entity decides which data to hand over to the centralised entity. 

The owner of the central model should be trusted by the other entities, because even though it no longer stores all data, the other entities still send data its way, and the central entity can technically choose what to do with this data (in theory it could still store it, after receiving it). However, less trust than with the fully centralised model is required, because each entity has the option not to share certain parts of its data, meaning this data remains 100% private. This is not possible when all data is stored centrally, where you always need to trust the central entity. 

The main advantage of this design compared to the fully centralised system is that each entity chooses which data it shares with the centralised entity.

The main advantage of this design compared to the fully decentralised system is that it is now possible to take advantage of the data of all entities, rather than just your own.

Disadvantages include:
- The entities should trust the central entity with the data they send to it.
- The central entity depends on other entities for the data it needs to train its models, and they might not cooperate
- The central entity should trust the other entities to some extent, because they could send over incorrect data

Considered from a ML point of view, there is an additional disadvantage: this architecture only works out of the box with iterative machine-learning models. An ML model can be trained iteratively if it does not need to have access to all data before starting training. Logistic regression, or the extension to neural networks, are iteratively trained by default. 
However, other models, such as those based on decision trees, are easier to train when all data is available beforehand. In this setting with decentralised data, the central entity forgets all data sent to it once it is processed, thus non-iterative methods become impossible to use, unless all data is obtained simultaneously.

Another, more subtle, technical ML point relates to whether it is possible to reconstruct the training data from the model. For this particular setting: imagine an adversary observes that an entity sends its data to the central model. If it can query the model beforehand and afterwards, it can quantify the incremental change caused by this new data and try to figure out what it was. Discussing this issue and its implications is out of scope here.

##### Illustrations:
- Minimal example:

        node illustrations/illustration_centralised_model_decentralised_data.js 

- Minimal example with a server:
    
        node illustrations/illustration_centralised_model_decentralised_data_with_server.js 


The running example illustrates that the central entity managed to learn the function well, despite only having access to the data at the moment of processing it. Each entity also has access to a better model than they would have had they only used their own data.

## Untrusted centralised model - decentralised data and training

The difference with the previous design is that here the model is trained by the decentralised entities, and that their data never leaves their premises. In particular: the central entity never processes the data from the other entities.

The role of the central entity is reduced to being a single source of truth for the model state, without performing any data processing.
It sends over the model when requested by some other entity, and replaces the model when that entity sends it back, retrained with its data.

The main advantage compared to the previous approach is that the central model can be much more adversary. If the central entity were to be untrustworthy, it would find it much harder to obtain the private data of the other entities, because it is no longer processed on its servers. 

The main disadvantage compared to the previous approach is that the non-central entities must be trusted more. In the previous approach, the central entity could still run some checks on the data received for training, to verify that it makes sense. Now it only receives a new model, and has no way of knowing which data was used to retrain it.

##### Illustration

        node illustrations/illustration_centralised_model_trained_in_decentralised_manner.js
