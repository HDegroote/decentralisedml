const express = require('express')
tfmodel = require("./tfmodel")
const {CentralisedModelOnlySystem} = require("./centralised_model_only_system");
const {Data} = require("./data")


const app = express()
const port = 3456
const system = new CentralisedModelOnlySystem()

app.listen(port, () => {
    console.log(`Server with centralised model (but no data) listening at http://localhost:${port}`)
})

app.use(express.json())


//DEVNOTE: consider ways to avoid code duplication with fully_centralised_server
app.post('/predict', async (req, res) => {
    const coordinatesKey = "xValues"
    console.log("Received /predict request")
    console.log("body: ", JSON.stringify(req.body))

    const coordinates = req.body[coordinatesKey];    
    const prediction = await system.predict(coordinates)

    result = JSON.stringify(prediction)
    console.log("Completed /predict request--returning", result)
    res.send(result)
})


app.post('/train', async (req, res) => {
    const dataKey = "data"
    console.log("Received /train request")
    console.log("body: ", JSON.stringify(req.body))
    data = new Data(req.body[dataKey])
    await system.trainModel(data)

    resStatus = 200
    console.log("Completed /train request--returning status", resStatus)
    res.sendStatus(resStatus)
})

