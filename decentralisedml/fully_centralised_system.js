const assert = require("assert")
tfmodel = require("./tfmodel")
data = require("./data")



class CentralisedSystem{
    model
    userData

    constructor(){
        this.model = new tfmodel.TfModel()
        this.userData = []
    }

    addUser(data){
        this.userData.push(data)
    }

    async trainModel(){
        let allDataAggregated = data.Data.createFromMultiple(this.userData)
        await this.model.train(allDataAggregated.xValues, allDataAggregated.yValues)
    }

    async predict(xValues){
        return await this.model.predict(xValues)
    }
}


function testCentralisedSystemFunctionality(){
    sys = new CentralisedSystem()
    assert.deepEqual(sys.userData, [])
    sys.addUser([[1,2,3], [4,5,6]])
    assert.deepEqual(sys.userData, [[[1,2,3],[4,5,6]]])
    sys.addUser([[7,8,9]])
    assert.deepEqual(sys.userData, [[[1,2,3],[4,5,6]], [[7,8,9]]])
}

testCentralisedSystemFunctionality()

module.exports.CentralisedSystem = CentralisedSystem
