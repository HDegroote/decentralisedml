const assert = require("assert")
tfmodel = require("./tfmodel")
data = require("./data")



class CentralisedModelOnlySystem{
    model

    constructor(){
        this.model = new tfmodel.TfModel()
    }

    async trainModel(data){
        await this.model.train(data.xValues, data.yValues)
    }

    async predict(xValues){
        return await this.model.predict(xValues)
    }
}


async function testCentralisedSystemFunctionality(){
    sys = new CentralisedModelOnlySystem()    
    await sys.trainModel(new data.Data([[1,1,1], [-1,-1,0]]))
    let prediction = -1
    prediction = await sys.predict([[10,10]])
    assert.deepEqual(prediction, [1])
    
    await sys.trainModel(new data.Data([[1,-1,1], [-1,1,0]])) 
    prediction = await sys.predict([[1,1], [-1,1]])
    assert.deepEqual(prediction, [1, 0])
}

async function runTests(){
    await testCentralisedSystemFunctionality()    
}
//runTests()

module.exports.CentralisedModelOnlySystem = CentralisedModelOnlySystem
