const express = require('express')
tfmodel = require("./tfmodel")
const {CentralisedSystem} = require("./fully_centralised_system");
const {Data} = require("./data")

const app = express()
const port = 3456
const system = new CentralisedSystem()

app.listen(port, () => {
    console.log(`Fully centralised server listening at http://localhost:${port}`)
})

app.use(express.json())


app.post('/predict', async (req, res) => {
    const coordinatesKey = "xValues"
    console.log("Received /predict request")
    console.log("body: ", JSON.stringify(req.body))

    const coordinates = req.body[coordinatesKey];
    //console.log(`xValues received: ${JSON.stringify(coordinates)}`)
    const prediction = await system.predict(coordinates)

    result = JSON.stringify(prediction)
    console.log("Completed /predict request--returning", result)
    res.send(result)
})

app.post('/adduser', async (req, res) => {
  const dataKey = "data"
  console.log("Received /adduser request")
  console.log("body: ", JSON.stringify(req.body))
  const data = req.body[dataKey];
  //console.log(`coordinates received: ${JSON.stringify(data)}`)
  await system.addUser(new Data(data))

  resStatus = 200
  console.log("Completed /adduser request--returning status", resStatus)
  res.sendStatus(resStatus)
})


app.get('/train', async (req, res) => {
  console.log("Received /train request")
  await system.trainModel()

  resStatus = 200
  console.log("Completed /train request--returning status", resStatus)
  res.sendStatus(resStatus)
})

