const assert = require("assert")


class Data{    
    data
      
    constructor(data){
        /**
            Assumes the last column of data is Y, and all previous columns the X
        **/
      this.data = data
    }
  
    static getCoors(data){
      return data.map(array => {
        const indexAfterLastCoor = array.length-1
        return array.slice(0, indexAfterLastCoor)
      })
    }
    
    static getClasses(data){
      return data.map(array => {
        const classIndex = array.length-1
        return array[classIndex]
      })
    }

    static createFromMultiple(datas){
      if(datas.length < 1){
        throw Error(`Can only aggregate data if there is at least one entry of data, but received '${datas}'`)
      }
      //assume length >= 1
      let allDataConcatenated = datas[0].data
      for(let i=1; i<datas.length; i++){
        allDataConcatenated = allDataConcatenated.concat(datas[i].data)
      }
      return new Data(allDataConcatenated)
    }
  
    get xValues(){
      return Data.getCoors(this.data)
    }
  
    get yValues(){
      return Data.getClasses(this.data)
    }
}

  


function testDataClass(){
  data = new Data([[1,2,3], [4,5,6]])

  assert.deepEqual(data.xValues, [[1,2], [4,5]])
  assert.deepEqual(data.yValues, [3,6])

  data2 = new Data([[-1,-2,-3]])
  aggregatedData = Data.createFromMultiple([data, data2])
  assert.deepEqual(aggregatedData.xValues, [[1,2], [4,5], [-1,-2]])
  assert.deepEqual(aggregatedData.yValues, [3,6,-3])
}

testDataClass()


module.exports.Data = Data