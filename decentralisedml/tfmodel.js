const tf = require("@tensorflow/tfjs-node") //'./node_modules/@tensorflow/tfjs-node/') //Must be node for some reason, see https://github.com/tensorflow/tfjs/issues/723 and be confused
const e = require("express")

//TODO open question: what happens if fit is called multiple times simultaneously?

class TfModel{
  constructor(){
    this.model = TfModel.create_and_compile_model()
  }

  async predict(coordinates){
    const tfstyle_coordinates = tf.tensor(coordinates)
    const prediction = this.model.predict(tfstyle_coordinates)
    const predicted_array = await prediction.array()
    const predicted_values = predicted_array.map(array => {
      return Math.round(array[0])
    })
    return predicted_values
  }

  static create_and_compile_model(){
    const res_model = tf.sequential({
        layers: [
            tf.layers.dense({
                inputShape: [2],
                units: 1,
                activation: 'sigmoid',
                kernelInitializer: 'randomUniform',
                biasInitializer: 'zeros'
            })
        ]
    })
    TfModel.compileModel(res_model)
    return res_model
  }

  static compileModel(model){
    model.compile({
      optimizer: tf.train.sgd(1), 
      loss:"binaryCrossentropy",
      metrics:['accuracy']
    }) 
  }
  
  async train(coordinates, classes, epochs=100 ){
    const tf_coordinates = tf.tensor(coordinates)
    const tf_classes = tf.tensor1d(classes)
    await this.model.fit(tf_coordinates, tf_classes, {epochs: epochs})
  } 

  async createDeepCopy(){
    //From https://stackoverflow.com/questions/68807412/clone-tensorflow-js-loaded-model
    const modelData = new Promise(resolve => this.model.save({save: resolve}));
    const newModel = await tf.loadLayersModel({load: () => modelData});    
    TfModel.compileModel(newModel)
    
    const res = new TfModel() //DEVNOTE: not so efficient since a blank model is created in constructor. Consider refactor
    res.model = newModel
    return res
  }

}

async function printModel(model){
  const weights = model.model.getWeights()
  for(x of weights){
    const values = await x.data()    
    console.log(values)
  }
}



module.exports.TfModel = TfModel
module.exports.printModel = printModel