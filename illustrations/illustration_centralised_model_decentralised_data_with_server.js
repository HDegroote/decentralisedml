child_process = require('child_process')
const axios = require("axios")
const {ExampleData, evaluateModelOnServer} = require("./running_example")
const {simulatePredictRequest} = require("./shared_requests")

port = 3456

async function simulateTrainRequest(data){
    response = await axios.post(
        `http://localhost:${port}/train`,
        {"data": data}
    )
    if(response.status != 200){
        throw Error("Not-200 http status " + response.status + " \nmsg: " + response.data)
    }
    else{
      console.log("Successfully simulated train request")
    }
}


async function main(){
    console.warn("Warning: this code creates a separate process for the server, but without proper code to clean up in case of error in the main process. So the server process is terminated if the code executes successfully. To terminate the server process manually (on ubuntu), first search the process id with 'sudo netstat -lpn |grep :3456' (3456 being the port nr) then terminate the correct process with 'kill 12345' (with 12345 the process id)")



    //Run server
    server_process = child_process.fork(__dirname + '/../decentralisedml/centralised_model_only_server.js'); 
    // Give the server time to init (note: if server launches only after this time, kill the dangling node process occupying the port and rerun with more allotted time)
    // From https://stackoverflow.com/questions/14249506/how-can-i-wait-in-node-js-javascript-l-need-to-pause-for-a-period-of-time
    await new Promise(resolve => setTimeout(resolve, 1000)); 

    //Setup the users (each with their private data)
    const exData = new ExampleData()
    user1 = exData.data1
    user2 = exData.data2

    //The first user sends its data to the server, which immediately uses it to update the model
    await simulateTrainRequest(user1.data)
    //Idem user 2
    await simulateTrainRequest(user2.data)

    await evaluateModelOnServer(`http://localhost:${port}/predict`)
    
    console.log("Finished demo. Shutting down server")
    await server_process.kill()            
}

main()