const {ExampleData, evaluateModel} = require("./running_example")
const {CentralisedModelOnlySystem} = require("../decentralisedml/centralised_model_only_system")


async function illustrateCentralisedModelTrainedInDecentralisedManner(){
    system = new CentralisedModelOnlySystem()

    //System contains model, but no data
    console.log("init model")
    await tfmodel.printModel(system.model.model)
     
    //Each user owns its own data, privately
    const exData = new ExampleData()
    user1Data = exData.data1
    user2Data = exData.data2

    // Whenever a user wants, it can request the model from the server and train it with its data
    // Note that concurrency issues are not considered here

    //User 1 trains the model with its data, on its own server
    const modelAtUser1 = await system.model.createDeepCopy()
    await modelAtUser1.train(user1Data.xValues, user1Data.yValues)
    system.model = await modelAtUser1.createDeepCopy()//Send trained model back to central sys


    console.log("Intermediary evaluation, after User1 used his data to train the model:")
    await evaluateModel(system.model)

    //Idem User 2
    const modelAtUser2 = await system.model.createDeepCopy()
    await modelAtUser2.train(user2Data.xValues, user2Data.yValues)
    system.model = await modelAtUser2.createDeepCopy()

    console.log("Model after both user1 and user2 user their data to train it: ")
    await tfmodel.printModel(system.model.model)    
    await evaluateModel(system.model)
    
  }
    
  
  illustrateCentralisedModelTrainedInDecentralisedManner()
  