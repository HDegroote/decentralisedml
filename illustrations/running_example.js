const assert = require("assert")
data = require("../decentralisedml/data")
const axios = require("axios")


class ExampleData{
    data1
    data2
    testData
  
    constructor(){
      const data1Content = []
      const data2Content = []
  
      const nrCoorsPerQuadrant = 3
      for(let i=1; i<nrCoorsPerQuadrant+1; i++){
        for(let j=i; j<nrCoorsPerQuadrant+1; j++){
          data1Content.push([i,j, 1])
          data1Content.push([-i,-j, 0])
          data2Content.push([-i,j, 0])
          data2Content.push([i,-j, 1])
  
        }
      }
    this.data1 = new data.Data(data1Content)
    this.data2 = new data.Data(data2Content)
    this.testData = new data.Data([[3,3,1], [-3, -3,0], [-3,3,0], [3,-3,1]])
    }  
  }

  function checkPredictionsAgainstTestData(predictions, testData){
    nrCorrect = 0 
    assert.equal(testData.yValues.length, predictions.length)
    for(let i=0; i<predictions.length; i++){
        if(predictions[i] == testData.yValues[i]){
            nrCorrect += 1
            console.log(`Correct prediction for (${testData.xValues[i]}): ${predictions[i]}`)
        }
        else{
            console.log(`Incorrect prediction for (${testData.xValues[i]}): ${predictions[i]}`)
        }
    }    
    console.log(`Correctly predicted ${nrCorrect}/${predictions.length}`)

  }

  async function evaluateModel(model){
    testData = new ExampleData().testData
    predictions = await model.predict(testData.xValues)
    checkPredictionsAgainstTestData(predictions, testData)
  }

  async function evaluateModelOnServer(entryPoint, xValuesKey = "xValues"){
    testData = new ExampleData().testData
    console.log("testdata in evaluate: ", testData.xValues)

    response = await axios.post(
      entryPoint,
      {
        [xValuesKey]: testData.xValues // https://stackoverflow.com/questions/3153969/create-object-using-variables-for-property-name 
      }
    )
    if(response.status == 200){
        predictions = response.data                
    }
    else{      
        throw Error("not-200 http status: " + response.status + " \nmsg: " + response.data)
    }

    checkPredictionsAgainstTestData(predictions, testData)    
  }


  module.exports.ExampleData = ExampleData
  module.exports.evaluateModel = evaluateModel
  module.exports.evaluateModelOnServer = evaluateModelOnServer