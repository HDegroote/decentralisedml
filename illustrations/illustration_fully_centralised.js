const {ExampleData, evaluateModel} = require("./running_example")
centralisedsystem = require("../decentralisedml/fully_centralised_system")

async function illustrateFullyCentralised(){
    system = new centralisedsystem.CentralisedSystem()

    //System contains model
    console.log("init model")
    await tfmodel.printModel(system.model.model)
     
    //System contains all user data
    const exData = new ExampleData()
    system.addUser(exData.data1)
    system.addUser(exData.data2)

    //System trains model internally
    await system.trainModel()
    console.log("Model after training: ")
    await tfmodel.printModel(system.model.model)
    
    await evaluateModel(system.model)
    
  }
  
  
  
  illustrateFullyCentralised()
  