const {ExampleData, evaluateModel} = require("./running_example")
centralisedsystem = require("../decentralisedml/fully_centralised_system")

async function illustrateFullyCentralised(){
    systemUser1 = new centralisedsystem.CentralisedSystem()
    systemUser2 = new centralisedsystem.CentralisedSystem()

    //Each system contains its own user data, covering only a subset of the instance space
    const exData = new ExampleData()
    console.log("System 1 training data, right-upper and left-lower quadrants: ", JSON.stringify(exData.data1))
    systemUser1.addUser(exData.data1)
    console.log("System 2 training data, left-upper and right-lower quadrants: ", JSON.stringify(exData.data2))
    systemUser2.addUser(exData.data2)

    //Each system trainss its model internally, without sharing data with other systems
    await systemUser1.trainModel()
    await systemUser2.trainModel()

    console.log("Models after training: ")
    console.log("Model for system 1")
    await tfmodel.printModel(systemUser1.model.model)
    console.log("Model for system 2")

    await tfmodel.printModel(systemUser2.model.model)

    console.log("Evaluation for system 1")
    await evaluateModel(systemUser1.model)
    console.log("Evaluation for system 2")
    await evaluateModel(systemUser2.model)
    
  }
  
  
  
  illustrateFullyCentralised()
  