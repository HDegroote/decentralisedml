child_process = require('child_process')
const axios = require("axios")
const {ExampleData, evaluateModelOnServer} = require("./running_example")
const {simulatePredictRequest} = require("./shared_requests")
port = 3456


async function simulateTrainRequest(){
    response = await axios.get(
        `http://localhost:${port}/train`
    )
    if(response.status != 200){
        throw Error("Not-200 http status " + response.status + " \nmsg: " + response.data)
    }
    else{
      console.log("Successfully simulated train request")
    }
}

async function simulateAddUserRequest(data){
    response = await axios.post(
        `http://localhost:${port}/adduser`,
        {
            "data": data
        }
    )
    if(response.status != 200){
        throw Error("Not-200 http status for adduser request " + response.status + " \nmsg: " + response.data)
    }
    else{
        console.log("Successfully simulated adduser request")
    }
}


async function main(){
    console.warn("Warning: this code creates a separate process for the server, but without proper code to clean up. So the server process is terminated if the code executes successfully. To terminate the server process manually (on ubuntu), first search the process id with 'sudo netstat -lpn |grep :3456' (3456 being the port nr) then terminate the correct process with 'kill 12345' (with 12345 the process id)")

    //Run server
    server_process = child_process.fork(__dirname + '/../decentralisedml/fully_centralised_server.js'); 
    // Give the server time to init (note: if server launches only after this time, kill the dangling node process occupying the port abd rerun with more allotted time)
    // From https://stackoverflow.com/questions/14249506/how-can-i-wait-in-node-js-javascript-l-need-to-pause-for-a-period-of-time
    await new Promise(resolve => setTimeout(resolve, 1000)); 

    //Add users: transfer data to server
    const exData = new ExampleData()
    await simulateAddUserRequest(exData.data1.data)
    await simulateAddUserRequest(exData.data2.data)

    // Train model (happens serverside)
    await simulateTrainRequest()

    let prediction = await simulatePredictRequest([[1,1]])
    console.log("lone prediction: ", prediction)

    await evaluateModelOnServer(`http://localhost:${port}/predict`)
    
    console.log("Finished fully centralised ML demo. Shutting down server")
    await server_process.kill()       
}


if (require.main === module) {
    main()
}


