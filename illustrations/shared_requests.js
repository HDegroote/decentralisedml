const axios = require("axios")


async function simulatePredictRequest(coordinates){
    response = await axios.post(
        `http://localhost:${port}/predict`,
        {
            "xValues": coordinates
        }

    )
    if(response.status == 200){
        prediction = response.data                
        console.log("prediction: ", prediction)
        return prediction
    }
    else{
        throw Error("http error with status" + response.status + " \nmsg: " + response.data)
    }
}

module.exports.simulatePredictRequest = simulatePredictRequest