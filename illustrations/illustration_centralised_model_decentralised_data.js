const {ExampleData, evaluateModel} = require("./running_example")
const {CentralisedModelOnlySystem} = require("../decentralisedml/centralised_model_only_system")

async function illustrateCentralisedModelDecentralisedData(){
    system = new CentralisedModelOnlySystem()

    //System contains model, but no data
    console.log("init model")
    await tfmodel.printModel(system.model.model)
     
    //Each user owns its own data, privately
    const exData = new ExampleData()
    user1 = exData.data1
    user2 = exData.data2

    // When a user wants to, it can pass its data to the server
    // Note that in an actual application, the server would most likely check if the user and its data are trusted before using the data

    //User 1 shares his data
    await system.trainModel(user1)
    //User 2 shares his data 
    await system.trainModel(user2)


    console.log("Model after training: ")
    await tfmodel.printModel(system.model.model)    
    await evaluateModel(system.model)
    
  }
  
  
  
  illustrateCentralisedModelDecentralisedData()
  